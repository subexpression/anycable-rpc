require 'rack'
require 'uri'
require 'anycable'
require 'redis'
require 'json'


class CustomConnection
  attr_reader :identifiers, :context, :request, :socket, :subscriptions

  def initialize(socket, identifiers, context)
    @socket = socket
    @identifiers = identifiers
    @context = context
    @request = Rack::Request.new(socket.env)
    @subscriptions = {}
    setup_redis
  end

  def command(message)
    puts " "
    puts "////////////////"
    puts "## command(message) ##"
    case message["command"]
    when "subscribe"
      puts "subscribe message"
    when "unsubscribe"
      puts "unsubscribe message"
    when "message"
      puts "message message"
    end
    puts "////////////////"
    puts " "
  end

  def receive_message(raw_message)
    message = JSON.parse(raw_message)
    command = message['command']
    data = message['data']

    handle_channel_command(command, data)
  end

  def handle_open
    # 
  end

  def handle_close
    # 
  end

  def handle_channel_command(identifier, command, data)
    puts " "
    puts "////////////////"
    puts "## handle_channel_command() ##"
    puts "identifier.inspect: " + identifier.inspect
    puts "command.inspect: " + command.inspect
    puts "data.inspect: " + data.inspect
    puts "////////////////"
    puts " "

    case command
    when 'subscribe'
      handle_subscribe(identifier, data)
    when 'unsubscribe'
      handle_unsubscribe(identifier, data)
    when 'message'
      handle_message(identifier, data)
    else
      # 
    end
  end

  def identifiers_json
    @identifiers.to_json
  end

  def handle_subscribe(identifier, data)
    channel_id = data['channel_id']
    puts "SUBSCRIBE CHANNEL ID: " + channel_id
    
    stream_name = "messaging_channel_#{channel_id}"
    @socket.subscribe(stream_name)
  end

  def handle_unsubscribe(identifier, data)
    channel_id = data['channel_id']
    puts "UNSUBSCRIBE CHANNEL ID: " + channel_id
  end

  def handle_message(identifier, data)
    channel_id = data['channel_id']
    message = data['message']
    puts "MESSAGE CHANNEL ID: " + channel_id
    puts "MESSAGE: " + message
  end

  private

  def setup_redis
    @redis = Redis.new(
      host: ENV['REDIS_SERVICE'],
      port: ENV['REDIS_PORT'].to_i,
      db: ENV['REDIS_DB'].to_i
    )
  end

end


class CustomConnectionFactory

  def initialize
    setup_redis
  end

  def setup_redis
    @redis = Redis.new(
      host: ENV['REDIS_SERVICE'],
      port: ENV['REDIS_PORT'].to_i,
      db: ENV['REDIS_DB'].to_i
    )
  end

  def call(socket, **options)
    puts " "
    puts "## call() ##"
    puts "socket.inspect: " + socket.inspect
    puts "options: " + options.inspect
    puts " "
    env = socket.env
    token = extract_token(env)
    puts "Token extracted: #{token.inspect}"

    return reject_unauthorized_connection unless token

    if (user_id = authenticate_user(token))
      puts "Authenticated user_id: #{user_id}"

      puts "fetch_user_data()"
      user_data = fetch_user_data(user_id) || {}
      puts "user_data.inspect: " + user_data.inspect
      puts "CustomConnection.new()"
      connection = CustomConnection.new(socket, { user_id: user_id }, user_data)
      puts "connection.inspect: " + connection.inspect
      puts "Connection created with user_data: #{user_data.inspect}"
      puts "connection.identifiers.inspect: " + connection.identifiers.inspect
      connection
    else
      puts "Failed to authenticate user with token: #{token}"
      reject_unauthorized_connection
    end
  end

  private

  def extract_token(env)
    puts " "
    puts "////////////////"
    puts "## extract_token(env) ##"
    puts "env.inspect: " + env.inspect
    anycable_env = env.instance_variable_get("@request_env")
    query_string = env['QUERY_STRING']
    puts "query_string: " + query_string.inspect  
    return nil unless query_string
    params = Rack::Utils.parse_nested_query(query_string)
    puts "params: " + params.inspect
    token = params['websocket_token']
    puts "token: " + token.inspect
    puts "////////////////"
    puts " "  
    raise "Token not found in request" unless token
  
    token
  rescue => e
    puts "Error extracting token: #{e.message}"
    nil
  end

  def authenticate_user(token)
    puts "Attempting to authenticate token: #{token}"
    user_id = @redis.get("websocket_token:#{token}")
    puts "User ID for token #{token}: #{user_id || 'not found'}"
    user_id
  rescue => e
    puts "Error fetching user from Redis: #{e.message}"
    nil
  end

  def fetch_user_data(user_id)
    user_data_json = @redis.get("user_data:#{user_id}")
    user_data = JSON.parse(user_data_json, symbolize_names: true) if user_data_json
    user_data
  end

  def reject_unauthorized_connection
    raise "Unauthorized connection attempt"
  end
end

AnyCable.connection_factory = CustomConnectionFactory.new
